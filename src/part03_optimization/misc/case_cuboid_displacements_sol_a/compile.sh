#!/bin/bash
#--------------------------------------------------------------------
# Benchmark 2d: Hexagon(al?) domain optimization. 
# Figure composed by the ratio norm(u)/u0 against a frame with the 
# equilibrium configuration (e.g. the optimized one). 
# This is done all in one-step scheme with PGF/TikZ. 
# If you want to compile standalone the figure. Set the environment
# variable;
# SOURCE=compileTikz_bis
# 
# Author: Nicolás Biocca
#--------------------------------------------------------------------

# compile figures individually (using tikz externalize) 
SOURCE=compileTikz
pdflatex -file-line-error -interaction=nonstopmode $SOURCE.tex
rm $SOURCE.{aux,bbl,blg,synctex.gz,out,auxlock}

# parse TikZ source filename.
# Ok, let's parse this the easy way (HARD CODE)
TIKZSOURCE=rectangle-Usol

# rename it
folderName=${PWD##*/}
mv $SOURCE.pdf $folderName.pdf



