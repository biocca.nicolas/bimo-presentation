# Biologically-Inspired Mesh Optimizer (BIMO) - Presentation

BIMO presentation (formerly PhD defense) written in `LaTeX` by means of 
`Beamer` class, editable through `TeXstudio` edito and ready to be presented 
via `pdfpc` presentation viewer.


## How to edit

Requirements: 
  - TeXstudio: integrated writting environment for LaTeX documents. 
  - `texlive`: A decent selection of the TeX Live packages. 
  - more packages problably.


On any ubuntu based should be enough the following instruction
```bash
sudo apt-get install --install-suggests –assume-yes texstudio texlive
```

`TODO`: Add session file `session.txss`

## How to present

Use `pdfpc` to properly reproduce the multimedia content.  

For more details, go to the official repo: [link to pdfpc](https://github.com/pdfpc/pdfpc)


## Preview

<img src="/uploads/d24738f8247e10eab0710fa763aa3be7/title_page.png" width="900">

<img src="/uploads/2c804bcba21606817f95022b89e1cbd4/presentation.gif" width="900">

## BIMO Results

### Friction Stir Spot Welding (FSSW)
<img src="/src/part06_mesh_motion_applications/img/fssw_bimo.mp4" width="1200">

### Heart Valve Inspired Benchmark
<img src="/src/part06_mesh_motion_applications/img/hvb_bimo.mp4" width="1200">
